import { Component, OnInit } from '@angular/core';
import { Bingo } from '../bingo';
import { Number } from '../number';

@Component({
  selector: 'app-bingo',
  templateUrl: './bingo.component.html',
  styleUrls: ['./bingo.component.scss']
})
export class BingoComponent implements OnInit {

  bingo: Bingo;
  constructor() { }

  ngOnInit() {
  	this.bingo = new Bingo();
  }

}
